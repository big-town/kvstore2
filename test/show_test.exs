defmodule ShowTest do
  @moduledoc """
  Это тесты для визуальной оценки
  """
  use ExUnit.Case, async: false
  require Kvstore2

  @tag :skip
  test "Show all table " do
    IO.puts("**************CREATE********************")

    for f <- 1..20 do
      Kvstore2.Storage.create("key" <> Integer.to_string(f), Integer.to_string(f + 10), f)
    end

    Kvstore2.Tools.show_table(Kvstore2.dets_store_table(), :dets)
    Kvstore2.Tools.show_table(Kvstore2.ets_sort_table(), :ets)
    # Kvstore2.Tools.show_table(Kvstore2.ets_tmp_table,:ets)
    dets_i = :dets.info(Kvstore2.dets_store_table())
    ets_i = Kvstore2.Tools.count_keys(Kvstore2.ets_sort_table())
    IO.puts("#{inspect(dets_i[:size])} == #{inspect(ets_i)}")
    # assert dets_i[:size] == ets_i[:size]
    IO.puts("**********************************")
    IO.inspect(:os.system_time(:seconds))

    IO.puts("**************DELETE********************")
    Process.sleep(3000)
    IO.inspect(:os.system_time(:seconds))

    Kvstore2.Storage.delete_expire_ttl()

    dets_i = :dets.info(Kvstore2.dets_store_table())
    ets_i = Kvstore2.Tools.count_keys(Kvstore2.ets_sort_table())
    IO.puts("#{inspect(dets_i[:size])} == #{inspect(ets_i)}")
    # assert dets_i[:size] == ets_i[:size]
    IO.puts("DETS")
    Kvstore2.Tools.show_table(Kvstore2.dets_store_table(), :dets)
    IO.puts("ETS")
    Kvstore2.Tools.show_table(Kvstore2.ets_sort_table(), :ets)
    dets_i = :dets.info(Kvstore2.dets_store_table())
    ets_i = Kvstore2.Tools.count_keys(Kvstore2.ets_sort_table())
    IO.puts("#{inspect(dets_i[:size])} == #{inspect(ets_i)}")

    # Kvstore2.Tools.show_table(Kvstore2.ets_tmp_table,:ets)
    IO.puts("**********************************")
  end

  @tag :skip
  test "Show all table after update " do
    IO.puts("*************UPDATE*********************")
    Kvstore2.Storage.update("key10", "qwqwqwq", 5)

    IO.inspect(:os.system_time(:seconds))
    dets_i = :dets.info(Kvstore2.dets_store_table())
    ets_i = Kvstore2.Tools.count_keys(Kvstore2.ets_sort_table())
    IO.puts("#{inspect(dets_i[:size])} == #{inspect(ets_i)}")
    # assert dets_i[:size] == ets_i[:size]

    Kvstore2.Tools.show_table(Kvstore2.dets_store_table(), :dets)
    Kvstore2.Tools.show_table(Kvstore2.ets_sort_table(), :ets)
    # Kvstore2.Tools.show_table(Kvstore2.ets_tmp_table,:ets)
    IO.puts("**********************************")
    IO.puts(Kvstore2.Storage.read("key10"))
    Kvstore2.Storage.delete("key10")
    IO.puts("************AFTER DELETE10**********************")
    Kvstore2.Tools.show_table(Kvstore2.dets_store_table(), :dets)
    Kvstore2.Tools.show_table(Kvstore2.ets_sort_table(), :ets)
    dets_i = :dets.info(Kvstore2.dets_store_table())
    ets_i = Kvstore2.Tools.count_keys(Kvstore2.ets_sort_table())
    IO.puts("#{inspect(dets_i[:size])} == #{inspect(ets_i)}")

    # Kvstore2.Tools.show_table(Kvstore2.ets_tmp_table,:ets)
  end
end
