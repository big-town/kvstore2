use Mix.Config

config :kvstore2,
  dets_store_table: :kvstorage2,
  ets_tmp_table: :ets_temp,
  ets_sort_table: :sort_table
