defmodule Kvstore2.Storage do
  @moduledoc """
  Функции для работы с KV хранилищем.
  Не грузим фрондендщиков и возвращаем только false или true, то есть получилось или нет.
  При чтении обновлении и вставке учитываем TTL
  """
  require Logger
  require Kvstore2

  @doc """
  Создает KV элимент
  Если элемент новый или ttl уже истек возвращаем true иначе false
  """
  @spec create(binary(), binary(), integer()) :: true | false
  def create(key, value, ttl) do
    delete_expire_ttl()
    expire = :os.system_time(:seconds) + ttl

    if :dets.insert_new(Kvstore2.dets_store_table(), {key, value, expire}) do
      unless :ets.insert_new(Kvstore2.ets_sort_table(), {expire, [key]}) do
        [{_, v_ets}] = :ets.lookup(Kvstore2.ets_sort_table(), expire)
        :ets.insert(Kvstore2.ets_sort_table(), {expire, v_ets ++ [key]})
      end

      true
    else
      false
    end
  end

  defp insert_expire_ets(expire, key) do
    case :ets.lookup(Kvstore2.ets_sort_table(), expire) do
      # Если данные с указанной датой существуют в ets то добавляем текущий ключ
      [{_, v_ets}] -> :ets.insert(Kvstore2.ets_sort_table(), {expire, v_ets ++ [key]})
      # Если данных с новой датой не содержалось в таблице пишем новые данные
      [] -> :ets.insert(Kvstore2.ets_sort_table(), {expire, [key]})
    end
  end

  @doc """
  Обновляем KV элемент при обновлении учитываем истекший TTL
  Если элемент существует и ttl не истек, то обновляем и возвращаем true иначе false
  """
  @spec update(binary(), binary(), integer()) :: true | false
  def update(key, value, ttl) do
    delete_expire_ttl()
    expire = :os.system_time(:seconds) + ttl

    case :dets.lookup(Kvstore2.dets_store_table(), key) do
      [{key, _, old_expire}] ->
        [{_, v_ets}] = :ets.lookup(Kvstore2.ets_sort_table(), old_expire)

        # Если ключ в ets один то просто изменяем ttl dets удаляем старый и создаем новый элемен в ets
        if length(v_ets) == 1 do
          :dets.insert(Kvstore2.dets_store_table(), {key, value, expire})
          :ets.delete(Kvstore2.ets_sort_table(), old_expire)
          insert_expire_ets(expire, key)
          true
          # Если ключ в ets один
        else
          # В dets  изменяем существующий ключ
          :dets.insert(Kvstore2.dets_store_table(), {key, value, expire})
          # Удаляем из списка v_ets текущий ключ
          new_v_ets = List.delete(v_ets, key)

          # И записываем назад оставшиеся дынные без текущего ключа
          :ets.insert(Kvstore2.ets_sort_table(), {old_expire, new_v_ets})
          insert_expire_ets(expire, key)

          true
        end

      _ ->
        false
    end
  end

  @doc """
  Читаем KV элемент при этом учитываем истекший TTL
  Если элемент существует и ttl не истек возвращаем true иначе false
  """
  @spec read(binary()) :: false | String
  def read(key) do
    delete_expire_ttl()
    res=:dets.lookup(Kvstore2.dets_store_table(), key)
    case res do
      [{_, v, _}] -> inspect(v)
      _ -> false
    end
  end

  @doc """
  Удаляем
  """
  @spec delete(binary) :: false | true
  def delete(key) do
    delete_expire_ttl()

    case :dets.lookup(Kvstore2.dets_store_table(), key) do
      [{_, _, old_ttl}] ->
        # Ключ есть и мы его удаляем
        :dets.delete(Kvstore2.dets_store_table(), key)
        [{_, v_ets}] = :ets.lookup(Kvstore2.ets_sort_table(), old_ttl)

        if length(v_ets) == 1 do
          :ets.delete(Kvstore2.ets_sort_table(), old_ttl)
        else
          new_v_ets = List.delete(v_ets, key)
          :ets.insert(Kvstore2.ets_sort_table(), {old_ttl, new_v_ets})
        end

        true

      _ ->
        false
    end
  end

  defp loop_delete(:"$end_of_table"), do: :ok

  defp loop_delete(key) do
    # Проходим по сортированной таблице
    [{ttl, v_ets}] = :ets.lookup(Kvstore2.ets_sort_table(), key)
    # Получаем следующий ключ для итерации
    key = :ets.next(Kvstore2.ets_sort_table(), key)

    # Если ttl больше системного то прерываем цикл, так как остальные ttl все старше (таблица отсортирована)
    # Иначе удаляем запись как в dets так и в ets
    if ttl > :os.system_time(:seconds) do
      loop_delete(:"$end_of_table")
    else
      :ets.delete(Kvstore2.ets_sort_table(), ttl)

      # В ets ключи из dets хранятся в виде списка проходим по каждому ключу
      Enum.map(v_ets, fn k -> :dets.delete(Kvstore2.dets_store_table(), k) end)
      loop_delete(key)
    end
  end

  @doc """
  Удаляем элемент с истекшим ttl
  """
  @spec delete_expire_ttl() :: :ok
  def delete_expire_ttl() do
    # Передаем первый ключ отсортированной таблицы
    loop_delete(:ets.first(Kvstore2.ets_sort_table()))
  end
end
