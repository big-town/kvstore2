defmodule Kvstore2.OTP do
  use GenServer
  require Kvstore2
  require Logger

  defp loop(:"$end_of_table", _, _), do: :ok

  defp loop(key, table_from, table_to) do
    [{k_f, _, t_f}] = :ets.lookup(table_from, key)

    # Сначала пытаемся вставить как новый элемент, в этом случае наличие будет проверено встроенными средствами ВМ
    unless :ets.insert_new(table_to, {t_f, [k_f]}) do
      [{_, v_t}] = :ets.lookup(table_to, t_f)
      :ets.insert(table_to, {t_f, [k_f] ++ v_t})
    end

    key = :ets.next(table_from, key)
    loop(key, table_from, table_to)
  end

  @doc """
  Функция обратного вызова для GenServer.init/1
  """
  @impl true
  def init(state) do
    # Process.flag(:trap_exit, true)
    :dets.open_file(Kvstore2.dets_store_table(), type: :set) |> IO.inspect()
    :ets.new(Kvstore2.ets_tmp_table(), [:set, :public, :named_table]) |> IO.inspect()
    :ets.new(Kvstore2.ets_sort_table(), [:ordered_set, :public, :named_table]) |> IO.inspect()
    # Составляем ets sort
    case :dets.to_ets(Kvstore2.dets_store_table(), Kvstore2.ets_tmp_table()) do
      Kvstore2.ets_tmp_table() ->
        key = :ets.first(Kvstore2.ets_tmp_table())

        case loop(key, Kvstore2.ets_tmp_table(), Kvstore2.ets_sort_table()) do
          :ok ->
            Logger.info("Fill sorting table ok")
            :ets.delete_all_objects(Kvstore2.ets_tmp_table())
            {:ok, state}

          _ ->
            Logger.error("Fill sorting table error!")
            :error
        end

      _ ->
        Logger.error("Convert dets to ets error!")
        :error
    end
  end

  @doc """
  Функции обратного вызова для GenServer.handle_call/3
  """
  @impl true
  def handle_call({:create, key, val, ttl}, _from, state) do
    value = Kvstore2.Storage.create(key, val, ttl)
    {:reply, value, state}
  end

  @impl true
  def handle_call({:update, key, val, ttl}, _from, state) do
    value = Kvstore2.Storage.update(key, val, ttl)
    {:reply, value, state}
  end

  @impl true
  def handle_call({:read, key}, _from, state) do
    value = Kvstore2.Storage.read(key)
    {:reply, value, state}
  end

  @impl true
  def handle_call({:delete, key}, _from, state) do
    value = Kvstore2.Storage.delete(key)
    {:reply, value, state}
  end

  def start_link(state \\ []) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
    # GenServer.start(__MODULE__, state, name: __MODULE__)
  end

  @spec send(binary) :: binary
  def send(msg) do
    res = GenServer.call(__MODULE__, msg)
    res
  end

  # Пытался сделать вежливое завершение не работает
  # Возможно решения нет когда ВМ крэшится этот колбэк не вызывается
  @impl true
  def terminate(reason, state) do
    :dets.close(Kvstore2.dets_store_table())
    IO.inspect("#{reason} #{state}")
    state
  end
end
