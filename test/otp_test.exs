defmodule OTPTest do
  use ExUnit.Case, async: false
  require Kvstore2

  test "OTP test" do
    IO.puts("OTP test")
    # Очищаем таблицы
    :ets.delete_all_objects(Kvstore2.ets_sort_table())
    :dets.delete_all_objects(Kvstore2.dets_store_table())
    # Пишем ключ с ттл 1
    Kvstore2.OTP.send({:create, "key10", "qqqqq", 1})
    # Этот ключ считается
    assert Kvstore2.OTP.send({:read, "key10"}) == "\"qqqqq\""
    Process.sleep(1000)
    # А этот нет
    assert Kvstore2.OTP.send({:read, "key10"}) == false

    Kvstore2.OTP.send({:create, "key10", "qqqqq", 100})
    Kvstore2.OTP.send({:delete, "key10"})
    # Этот тоже не считается так как был удален
    assert Kvstore2.OTP.send({:read, "key10"}) == false

    # Пытаемся создать уже существующий ключ
    Kvstore2.OTP.send({:create, "key10", "qqqqq", 100})
    assert Kvstore2.OTP.send({:create, "key10", "qqqqq", 100}) == false

    # Пишем ключ с ттл 1
    Kvstore2.OTP.send({:create, "key20", "qqqqq", 1})
    Process.sleep(1000)
    # Пишем ключ с ттл 1 и снова успех, так как ттл истек
    assert Kvstore2.OTP.send({:create, "key20", "qqqqq", 1}) == true

    #:dets.close(Kvstore2.dets_store_table)
  end
end
